/**
 * 扫描器类
*/
export default class Scanner {
    constructor(templateStr) {
        this.templateStr = templateStr; // 模板字符串
        this.pos = 0; // 指针索引
        this.tail = templateStr; // 尾巴-还未扫码的字符串
    }
    // 功能弱，就是走过指定的内容，没有返回值
    scan(tag) {
        if (this.tail.indexOf(tag) === 0) {
            // tag有多长，比如{{长度是2，就让指针后移多少位
            this.pos += tag.length;
            // 尾巴也要变
            this.tail = this.templateStr.substring(this.pos);
        }
    }
    // 让指针进行扫码，直到遇见指定的结束内容，并且能够返回结束之前路过的文字
    scanUtil(stopTag) {
        // 记录一下执行本方法的时候pos的值
        const pos_backup = this.pos;
        // 当尾巴的开头不是stopTag的时候，就说明没有扫描到stopTag
        while (this.tail.indexOf(stopTag) !== 0 && this.eos()) {
            this.pos++;
            // 改变尾巴为从当前指针这个字符开始，到最后的全部字符
            this.tail = this.templateStr.substring(this.pos);
        }
        return this.templateStr.substring(pos_backup, this.pos);
    }
    // 指针索引必须小于模板字符串长度，避免死循环
    eos() {
        return this.pos < this.templateStr.length;
    }
}
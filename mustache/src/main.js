import parseTemplateToTokens from "./parseTemplateToTokens";
import nestedTokens from "./nestTokens";

// 全局提供TemplateEngine对象
window.TemplateEngine = {
    // 渲染方法
    render(templateStr, data) {
        // 调用parseTemplateToTokens函数，让模板字符串转化为tokens数组
        var tokens = parseTemplateToTokens(templateStr);
        console.log(nestedTokens(tokens));
    }
}
// TemplateEngine.render("<h1>今天学习了{{thing}}, 好{{mood}}。</h1>");
TemplateEngine.render(`<ol>{{#arr}}<li><div class='hd'>{{name}}的基本信息</div><div class='bd'><p>姓名：{{name}}</p><p>年龄：{{age}}</p><p>爱好：</p><ol>{{#hobbies}}<p><li>{{.}}</li></p>{{/hobbies}}</ol></div></li>{{/arr}}</ol>`);
/**
 * 将模板字符串转化为tokens数组
 */
import Scanner from "./Scanner";

export default function parseTemplateToTokens(templateStr) {
    var tokens = [];
    var words;
    // 实例化一个扫码器，构造时候提供一个参数，这个参数就是模板字符串
    var scanner = new Scanner(templateStr);
    // 让扫描器工作
    while(scanner.eos()) {
        // 收集开始标记之前的文字
        words = scanner.scanUtil("{{");
        if (words !== "") {
            // 存起来,存为文本形式
            tokens.push(['text', words]);
        }
        // 过双大括号
        scanner.scan("{{");
        // 收集结束标记出现之前的文字
        words = scanner.scanUtil("}}");
        if (words !== "") {
            // 这个words就是{{}}中间的内容，判断一下首字符
            if (words[0] === "#") {
                // 说明为特殊组格式，存起来，从下标为1的项开始存，因为下标为0的项是#
                tokens.push(["#", words.substring(1)]);
            } else if (words[0] === "/") {
                // 说明为特殊组格式，存起来，从下标为1的项开始存，因为下标为0的项是/
                tokens.push(["/", words.substring(1)]);
            } else {
                // 存起来，存为名称形式
                tokens.push(['name', words]);
            }
        }
        // 过双大括号
        scanner.scan("}}");
    }
    return tokens;
}
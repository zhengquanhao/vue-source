const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    mode: "production", // 开发模式，可选production，development
    entry: path.resolve(__dirname, "./src/main.js"), // 入口文件
    output: {
        path: path.resolve(__dirname, './dist'), // 打包生成的文件地址，必须是绝对路径
        filename: "[name].build.js" // 生成的文件名
    },
    devServer: {
        compress: true, // 是否压缩
        hot: true, // 不写默认也带热更新的效果
        host: "127.0.0.1",
        port: "8080",
        open: true,
        stats: 'errors-warnings' // 只在发生错误或有新的编译时输出
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html", // 生成的文件名
            template: path.resolve(__dirname, "./public/index.html"), // 模板html
            favicon: path.resolve(__dirname, "./public/favicon.ico") // 图标
        })
    ]
}